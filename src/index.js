var mongoose = require('mongoose');
var models = {};

var init = function(config, cb){
  if (!config.dbUrl){
    return console.error("Sin la URL de la BBDD es imposible arrancar");
  }
  //var mongodbURL = "mongodb://localhost:27017/nodetest2";
  var mongodbURL = config.dbUrl;
  mongoose.connect(mongodbURL, {user:config.dbUser, pass:config.dbPassword});
  var db = mongoose.connection;

  db.on('error',function (err) {
    console.log('Mongoose default connection error: ' + err);
  });

  db.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
  });

  db.once('open', function () {
    console.log('Connection successful to: ' + mongodbURL);
    models.userModel = require("./User").userModel;
    models.companyModel = require("./Company").companyModel;
    models.locationModel = require("./Location").locationModel;
    models.deviceModel = require("./Device").deviceModel;
    models.gatewayModel = require("./Gateway").gatewayModel;
    models.sensingModel = require("./Sensing").sensingModel;
    models.holidayModel = require("./Holiday").holidayModel;
    models.periodModel = require("./Period").periodModel;
    models.productionModel = require("./Production").productionModel;
    models.alarmModel = require("./Alarm").alarmModel;
    models.modelModel = require("./Model").modelModel;
    models.monthRequestModel = require("./MonthRequest").monthRequestModel;
    console.log("Modelos asignados");
    cb();
  });

  process.on('SIGINT', function() {
    mongoose.connection.close(function () {
      console.log('Mongoose default connection disconnected through app termination');
      process.exit(0);
    });
  });
  
};

exports.models = models;
exports.init = init;
