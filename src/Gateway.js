var mongoose = require('mongoose');
var utils = require('./utils');
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
// Gateway schema
var Gateway = new Schema({
  name: { type: String, required: true},
  company: { type: ObjectId, required: true, ref: 'Company'},
  location: { type: ObjectId, required: true, ref: 'Location'},
  description: { type: String},
  certificate: {type: String},
  networkId:{type: Number, default: 1},
  networkDeviceId: {type: Number, default: 80},
  enabled: { type: Boolean, default: true },
});

Gateway.index({ "location": 1, "name" : 1 }, { unique: true });

utils.consistency (Gateway, 'location');
utils.consistency (Gateway, 'company');

Gateway.statics.findByName = function(name,cb){
  this.findOne({ name: name }, cb);
};

Gateway.statics.findByCert = function(cert,cb){
  this.findOne({ certificate: cert }, cb);
};

Gateway.statics.updateById = function(id,update,cb){
  this.update({_id : id}, { $set: update} ,cb);
};

Gateway.statics.removeById = function(id,cb){
  this.remove({_id : id},cb);
};

Gateway.statics.create = function(data, cb){
  x = new model(data);
  x.save(cb);
};

//Define Models
var model = mongoose.model('Gateway', Gateway);

// Export Models
exports.gatewayModel = model;

