var mongoose = require('mongoose');
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
// Holiday schema
var Holiday = new Schema({
  day: { type: Number, required: true},
  month: { type: Number, required: true},
  year: { type: Number, required: true},
  description: {type: String},
  enabled: { type: Boolean, default:true},
});

Holiday.statics.removeById = function(id,cb){
  this.remove({_id : id},cb);
};

Holiday.statics.create = function(data, cb){
  x = new model(data);
  x.save(cb);
};

Holiday.statics.updateById = function(id,update,cb){
  this.update({_id : id}, { $set: update} ,cb);
};

//Define Models
var model = mongoose.model('Holiday', Holiday);



// Export Models
exports.holidayModel = model;

