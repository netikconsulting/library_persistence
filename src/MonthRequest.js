var mongoose = require('mongoose');
var moment = require('moment');
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
// MonthRequest schema
var MonthRequest = new Schema({
  company: { type: ObjectId, required: true, ref: 'Company'},
  month: { type: Number, required: true},
  year: { type: Number, required: true},
  gwQ: {type: Number, default:0},
  gwI: {type: Number, default:0},
  gwR: {type: Number, default:0},
  front: {type: Number, default:0}
});

MonthRequest.statics.inc = function(company,bucket){
  var now = moment();
  var year = now.year();
  var month = now.month()+1;
  var inc = {};
  inc[bucket] = 1;
  console.log("Incrementando estadisticas en", company, bucket);
  model.update(
    {company:company,month:month,year:year},
    {$inc:inc},
    {upsert:true}, function (err, result){
      if (err){console.log("Error updating month requests statistics", err);}
    }
  );
};


//Define Models
var model = mongoose.model('MonthRequest', MonthRequest);



// Export Models
exports.monthRequestModel = model;

