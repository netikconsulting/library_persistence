var mongoose = require('mongoose');

var bcrypt = require('bcrypt');
var SALT_WORK_FACTOR = 10;

var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var async = require('async');
var utils = require('./utils');


// User schema
var User = new Schema({
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true, select: false },
    company: { type: ObjectId, required: true, ref: 'Company'},
    fullName: { type: String, required: true },
    roles: {type: [String], required: true},
    enabled: { type: Boolean, default: true },
    created: { type: Date, default: Date.now },
    lastLogin: { type: Date},
    currentLogin: { type: Date},
    lastPasswordChange: { type: Date, default: Date.now },
    favoriteDevice: { type: ObjectId, required: false, ref: 'Device'},
});


// Bcrypt middleware on UserSchema
var bcryptData = function(data,cb){
  bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
    if (err) return cb(err);

    bcrypt.hash(data, salt, function(err, hash) {
        if (err) {return cb(err); }
        cb(null, hash);
    });
  });
};

User.pre('save', function(next) {
  var user = this;

  if (!user.isModified('password')) return next();
  bcryptData(user.password,function(err,hash){
        if (err) return next(err);
        user.password = hash;
        next();
   });
});

utils.consistency (User, 'company');

//Password verification
User.methods.comparePassword = function(password, cb) {
    bcrypt.compare(password, this.password, cb);
};


User.statics.changePassword = function(userId, newPassword, cb) {
  model.findById(userId, function (err,user){
    if (err){return cb(err);}
    if (!user){console.log('Unknown user');return cb('Unknown user');}
    user.password = newPassword;
    user.lastPasswordChange = new Date();
    user.save(cb);
  });
};

User.methods.getId = function() {
  return this._id;
};

User.statics.fullUser = function(req,cb){
  this.findOne(req,function (err, item) {
    if (err) {cb(err);}
    if (!item) {cb('No existe usuario');}
    item.fullCompany(function (err,result){
      if (err) {cb(err);}
      if (!result) {cb('No existe la compañía del usuario');}
      item = item.toObject();
      item.companyName = result.name;
      cb(null,item);
    });
  });
};

User.statics.touchLogin = function(userId, cb){
  model.findById(userId, function (err,user){
    if (err){return cb(err);}
    if (!user){console.log('Unknown user');return cb('Unknown user');}
    user.lastLogin = user.currentLogin;
    user.currentLogin = new Date();
    user.save(function(err, data){
       if (err) {return cb(err);}
       cb(null, data);
    });
  });
};

User.statics.findByName = function(name,cb){
  this.findOne({ username: name }, '+password', cb);
};

User.statics.updateById = function(id,update,cb){
  console.log("persistence", id, update);
  this.update({_id : id}, { $set: update} ,cb);
};

User.statics.removeById = function(id,cb){
  this.remove({_id : id},cb);
};

User.statics.create = function(data, cb){
  x = new model(data);
  x.save(cb);
};

User.methods.fullCompany = function(cb){
  console.log("Buscando compañía con _id",this.company);
  var locals={};
  companyModel = require("./Company").companyModel;
  companyModel.findOne({_id : this.company}, cb);
  console.log("despues del find");
};

//Define Models
var model = mongoose.model('User', User);



// Export Models
exports.userModel = model;
exports.userSchema = User;

