var api={};

var models=null;

api.consistency = function(referencingSchema, referencingField, referencedSchemaName){
  init();
  if (!referencedSchemaName){referencedSchemaName = referencingField;}
  var path = referencingSchema.path(referencingField);
  if (path){
    path.validate(function (value, respond) {
      models[referencedSchemaName].findOne({_id: value}, function (err, doc) {
        if (err || !doc) { respond(false);}
        else {respond(true);}
      });
    }, referencedSchemaName+' non existent');
  }
};

// init is made in order to avoid loop requires
var init= function(){
  if (models){return;}
  models = {
    company:   require('./Company').companyModel,
    location:  require('./Location').locationModel,
    gateway:   require('./Gateway').gatewayModel,
    device:    require('./Device').deviceModel
  };
};

module.exports=api;
