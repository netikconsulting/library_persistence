var mongoose = require('mongoose');
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
var utils = require('./utils');

var Production = new Schema({
  device: {type: ObjectId, required: true, ref: 'Device'},
  tstamp: {type: Date, required:true},
  intervalType: {type:String,enum:['quarter','hour','day','week','month'], required:true},
  production: {type: Number, required:true},
  energy: {type: Number, required:true},
  energyCost: {type: Number},
  energyCalculated: {type:Boolean, default:true}
});

utils.consistency (Production, 'device');

Production.statics.updateById = function(id,update,cb){
  this.update({_id : id}, { $set: update} ,cb);
};

Production.statics.removeById = function(id,cb){
  this.remove({_id : id},cb);
};

Production.statics.create = function(data, cb){
  x = new model(data);
  x.save(cb);
};

Production.statics.filter = function (filter){
  if (!filter.sort){filter.sort="tstamp";}
  var f = {"device":{"$in":filter.devices},"tstamp":{"$gte":filter.start,"$lt":filter.end}};
  var fieldsLaw = "-__v";
  var result = this.find(f, fieldsLaw);
  if (filter.skip){result = result.skip(filter.skip);}
  if (filter.limit){result = result.limit(filter.limit);}
  return result.sort(filter.sort);
};

Production.statics.fcount = function (filter, cb){
  var f = {"device":{"$in":filter.devices},"tstamp":{"$gte":filter.start,"$lt":filter.end}};
  this.count(f, cb);
};

Production.statics.stream = function (filter){
  return this.filter(filter).stream();
};

//Define Models
var model = mongoose.model('Production', Production);

// Export Models
exports.productionModel = model;

