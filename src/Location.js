var mongoose = require('mongoose');
var utils = require('./utils');
var async = require("async");
var moment = require("moment");


var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var periodCostHistoricSchema = new Schema({
  created: { type: Date, default: Date.now },
  periodCosts:[Number]
});

var indexedRateSchema = {
  date:{type:Date, required:true},
  hours:[Number]  // must be length 24
};

// Location schema
var Location = new Schema({
  name: { type: String, required: true},
  company: { type: ObjectId, required: true, ref: 'Company'},
  description: {type: String},
  address: {type:String},
  periodRate: { type: ObjectId, ref: 'Period'},
  periodCosts:[Number],
  periodCostHistoric: {type:[periodCostHistoricSchema],select: false},
  indexedRateHistoric:{type:[indexedRateSchema], select: false}
});

Location.index({ "company": 1, "name" : 1 }, { unique: true });


Location.pre('save', function(next) {
  var location = this;
  if (!location.isModified('periodCosts')){return next();}
  if (!location.periodCostHistoric){ location.periodCostHistoric = [];}
  location.periodCostHistoric.push({periodCosts:location.periodCosts});
  next();
});

utils.consistency (Location, 'company');

Location.statics.findByName = function(name,cb){
  this.findOne({ name: name }, cb);
};

Location.statics.updateById = function(id,update,cb){
  this.update({_id : id}, { $set: update} ,cb);
};

Location.statics.removeById = function(id,cb){
  this.remove({_id : id},cb);
};

Location.statics.updatePeriodCosts = function(id,costs, cb){
  /*model.findByIdAndUpdate(id, {$set: {periodCosts:costs}},function(err){
    if (err){console.log(err);return cb(err);}
    model.findByIdAndUpdate(id, {$push: {periodCostHistoric:{periodCosts:costs}}}, cb);
  });*/
  model.findById(id, function(err, doc){
    if (err){return cb(err);}
    if (doc){
      doc.periodCosts = costs;
      doc.save(cb);
    } else {cb('Unkown location id',id);}
  });
};

Location.statics.create = function(data, cb){
  x = new model(data);
  x.save(cb);
};


Location.statics.addIndexedRate = function(id, date, hours, done){
  date = moment(date).startOf("day");
  var updateSensing = false;
  async.series(
    [
      function(cb){model.findByIdAndUpdate(id, {$unset:{periodRate:""}}, cb);},
      function(cb){
        model.findIndexedRate(id, date, function(err,rate,info){
          if (err){return cb(err);}
          console.log("Comparando las fechas", date.format(), info?info.date:null);
          if (info && date.isSame(info.date)){
            //Deprecated -> return cb("Tarifa previa existente para ese día");
            updateSensing = true;
            model.update(
              { _id: id },
              { $pull: { 'indexedRateHistoric': { date: date.toDate()} } },
              cb
            );
          } else {
           cb();
          }
        });
      },
      function(cb){model.findByIdAndUpdate(id, {$push:{indexedRateHistoric: {date:date.toDate(), hours:hours}}}, cb);},
      function(cb){
        if (date.isAfter(moment())){return cb();}
          updateSensing = true;
        cb();
      }
    ],
    function(err, result){
      if (err){return done(err);}
      done(null,{updateSensing:updateSensing, date:date.toDate()});
    }
  );
};

Location.statics.findIndexedRate = function(id, date, cb){
  var sdate = (moment(date).startOf("day")).toDate();
  id = new mongoose.Types.ObjectId(id);
  model.aggregate([
    {$match:{_id:id}},
    {$project:{indexedRateHistoric:1}},
    {$unwind:"$indexedRateHistoric"},
    {$match:{"indexedRateHistoric.date":{$lte:sdate}}},
    {$sort:{"indexedRateHistoric.date":1}},
    {$group:{_id:id,date:{$last: "$indexedRateHistoric.date"}, hours:{$last: "$indexedRateHistoric.hours"}}}
  ], function(error, result){
    console.log("Result en findIndexedRate", JSON.stringify(result));
    if (error){return cb(error);}
    if (!result.length){return cb();}
    result = result[0];
    var hour = moment(date).diff(sdate,"hours");
    cb(null,result.hours[hour]/1000,{type:"indexed",date:result.date});
  });
};

//Define Models
var model = mongoose.model('Location', Location);

// Export Models
exports.locationModel = model;

