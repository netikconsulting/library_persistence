var mongoose = require('mongoose');
var Schema = mongoose.Schema,
    moment = require("moment"),
    ObjectId = Schema.ObjectId;

var preSchema = {
  device: {type: ObjectId, required: true, ref: 'Device'},
  tstamp: {type: Date, required:true},
  // kind 2 Quarter Sensing, kind 1 inmediate sensing
  kind: { type: Number, default: 1},
  energy: Number,
  eReactive: Number,
  duration: Number,
  energyCost: Number,  //Calculated

  temperatureR: Number,
  temperatureS: Number,
  temperatureT: Number,
  temperatureN: Number,

  instantCost: Number,  // Calculated
  harmConsider: Number
};
// Instant sensing with stats
var statsSensing = [
  "voltageR", "voltageS", "voltageT", "voltageN",
  "intensityR", "intensityS", "intensityT", "intensityN",
  "cosPhiR", "cosPhiS", "cosPhiT",
  "pAparent", "pActive", "pReactive",
  "harmDistVolt", "harmDistInt"];

statsSensing.forEach(function(key){
  preSchema[key]=Number;
  preSchema[key+"Avg"]=Number;
  preSchema[key+"Max"]=Number;
});

var Sensing = new Schema(preSchema);

Sensing.set('autoIndex', true);

Sensing.index({tstamp:1}, {name:"sensing-tstamp"}, function(err){
  if (err){return console.log("Error creando Sensings Index", err);}
  console.log("Sensings tstamp Index creado correctamente");
});


Sensing.statics.removeById = function(id,cb){
  this.remove({_id : id},cb);
};

Sensing.statics.create = function(data, cb){
  x = new model(data);
  x.save(cb);
};

Sensing.statics.filter = function (filter){
  if (!filter.sort){filter.sort="tstamp";}
  var f = mongoFilter(filter);
  var fieldsLaw = "-_id";
  if (filter.fields){fieldsLaw = fieldsLaw +" tstamp device "+ filter.fields;}
  else {fieldsLaw = fieldsLaw + " -kind -company -__v";}
  console.log("Selecting "+fieldsLaw);
  console.log("Filtering "+JSON.stringify(f));
  console.log("Sorting", filter.sort);
  var result = this.find(f, fieldsLaw);//.sort(filter.sort);
  //if (filter.fields){console.log("Selecting "+filter.fields+);result = result.select(filter.fields+" tstamp device");}
  if (filter.skip){result = result.skip(filter.skip);}
  if (filter.limit){result = result.limit(filter.limit);}
  return result;
};

Sensing.statics.fcount = function (filter){
  var f = mongoFilter(filter);
  var result = this.count(f);
  return result;
};

Sensing.statics.stream = function (filter, cb){
  console.log("filter.group", filter.group);
  if (filter.group){return hourExpress(filter, cb);}
  var stream = this.filter(filter).stream();
  //stream.pause();
  //console.log("Stream en pausa");
  cb(null, stream);
};

var ArrayStream = require('arraystream');


var hourExpress = function(filter, cb){
  var start = moment(filter.start).toDate();
  var end = moment(filter.end).toDate();
  var devices=[];
  for (var i = filter.devices.length - 1; i >= 0; i--) {
    devices.push(new mongoose.Types.ObjectId(filter.devices[i]));
  }
  var f = {"device":devices[0],"tstamp":{"$gte":start,"$lt":end}};
  if (filter.kind) {f.kind = filter.kind;}
  
  var shift = 3600000;
  if (moment(filter.start).isDST()){
    shift = shift * 2;
  }

  var p = { "y":{$year:[{$add:["$tstamp",shift]}]},
    "m":{$month:[{$add:["$tstamp",shift]}]},
    "w":{$week:[{$add:["$tstamp",shift]}]},
    "d":{$dayOfMonth:[{$add:["$tstamp",shift]}]},
    "h":{$hour:[{$add:["$tstamp",shift]}]},
    "tstamp": 1};

  var g = {"_id": { "year":"$y","month":"$m","day":"$d","hour":"$h"},
      tstamp:{$first: "$tstamp"}};
  if (filter.group == "day"){
    g["_id"] = {"year":"$y","month":"$m","day":"$d"};
  } else if (filter.group == "week"){
    g["_id"] = {"year":"$y","month":"$m","week":"$w"};
  } else if (filter.group == "month"){
    g["_id"] = {"year":"$y","month":"$m"};
  } else if (filter.group == "year"){
    g["_id"] = {"year":"$y"};
  }

  if (filter.fields){
    var sp = filter.fields.split(" ");
    var a = [];
    for (i = sp.length - 1; i >= 0; i--) {
      p[sp[i]]=1;
      var gk = "$avg";
      if (["energy","energyCost","eReactive"].indexOf(sp[i])>=0){gk="$sum";}
      g[sp[i]]={};
      g[sp[i]][gk] = "$"+sp[i];
      var j = {};
      j[sp[i]]={$ne: null };
      a.push(j);
    }
    f.$or = a;
  }
  return model.aggregate([
    {$match:f},
    {$sort:{"tstamp":1}},
    {$project:p},
    {$group:g},
    {$sort:{"tstamp":1}},
  ],function(err, result){
    if (err) {cb(err);}
    console.log("Sensing Express. Hay "+result.length+" resultados.");
    if (moment(filter.start).isDST() && !moment(filter.end).isDST()){
      result.pop();
      for (var i = 0; i < result.length; i++) {
        var d = moment(result[i].tstamp);
        if (!d.isDST()){
          d.add(1,"hours");
          result[i].tstamp = d.toDate();
        }
      }
    }
    cb(null,ArrayStream.create(result));
  });
};

var mongoFilter = function(filter){
  var f = {"device":{"$in":filter.devices},"tstamp":{"$gte":filter.start,"$lt":filter.end}};
  if (filter.kind) {f.kind = filter.kind;}
  if (filter.fields){
    var sp = filter.fields.split(" ");
    var a = [];
    for (var i = sp.length - 1; i >= 0; i--) {
      var j = {};
      j[sp[i]]={$ne: null };
      a.push(j);
    }
    f.$or = a;
  }
  return f;
};

Sensing.statics.consumptionCost = function(device, start, end, cb){
  var match = {
    "device": new mongoose.Types.ObjectId(device),
    "tstamp":{"$gte":start.toDate(),"$lt":end.toDate()}
  };
  var group = {
    _id: null,
    totalCost: { $sum: "$energyCost"},
    totalEnergy: { $sum: "$energy"}
  };
  this.aggregate({$match:match},{$group:group},function(err, result){
    if (err){return cb(err);}
    var cost = 0;
    if (result.length && result.length>0){cost = result[0];}
    cb(null, cost);
  });
};

Sensing.statics.totalCost = function(device, start, end, cb){
  var match = {
    "device": new mongoose.Types.ObjectId(device),
    "tstamp":{"$gte":start.toDate(),"$lt":end.toDate()}
  };
  var group = {
    _id: null,
    totalCost: { $sum: "$energyCost"},
    totalEnergy: { $sum: "$energy"}
  };
  this.aggregate({$match:match},{$group:group},function(err, result){
    if (err){return cb(err);}
    var cost = {totalCost:0, totalEnergy:0};
    if (result.length && result[0].totalCost){cost = result[0];}
    cb(null, cost);
  });
};

//Define Models
var model = mongoose.model('Sensing', Sensing);

// Export Models
exports.sensingModel = model;

