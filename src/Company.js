var mongoose = require('mongoose');
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
    
// Company schema
var Company = new Schema({
  name: { type: String, required: true, unique: true },
  enabled: {type:Boolean, default:true},
  nif: { type: String, required: true},
  country: { type: String, required: true},
  city: { type: String, required: true},
  province: { type: String, required: true},
  address: { type: String, required: true},
  phone: { type: String, required: true},
  paymentMode: { type: String},
  created: { type: Date, default: Date.now },
  logo: {type: String},
  masterCompany: { type: ObjectId, required: false, ref: 'Company'}
});

Company.statics.findByName = function(name,cb){
  this.findOne({ name: name }, cb);
};

Company.statics.updateById = function(id,update,cb){
  this.update({_id : id}, { $set: update} ,cb);
};

Company.statics.removeById = function(id,cb){
  this.remove({_id : id},cb);
};

Company.statics.create = function(data, cb){
  x = new model(data);
  x.save(cb);
};


//Define Models
var model = mongoose.model('Company', Company);

// Export Models
exports.companyModel = model;

