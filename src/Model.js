var mongoose = require('mongoose');
var utils = require('./utils');
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
// Model schema
var Model = new Schema({
  name: { type: String, required: true},
  company: { type: ObjectId, required: true, ref: 'Company'},
  device: { type: ObjectId, required: true, ref: 'Location'},
  kind: { type: String, enum:["energy","cost"], default: "energy"},
  description: { type: String},
  enabled: { type: Boolean, default: true },
  created: { type: Date, default: Date.now },
  // Length should be 24x4
  quarters:{
    type: [{type:String, enum:["L","M","H","N"]}],
    required: true,
    validator: function validateLength(values){
      return values && (values.length == 96);
    }
  },
  lmh:{
    type: [Number],
    required: true,
    validator: function validateLength(values){
      return values && (values.length == 3);
    }
  }
});

Model.index({ "device": 1, "name" : 1 }, { unique: true });

utils.consistency (Model, 'device');
utils.consistency (Model, 'company');

Model.statics.updateById = function(id,update,cb){
  this.update({_id : id}, { $set: update} ,cb);
};

Model.statics.removeById = function(id,cb){
  this.remove({_id : id},cb);
};

Model.statics.create = function(data, cb){
  x = new model(data);
  x.save(cb);
};

//Define Models
var model = mongoose.model('Model', Model);

// Export Models
exports.modelModel = model;

