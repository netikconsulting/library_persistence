var mongoose = require('mongoose'),
    utils = require('./utils'),
    moment = require('moment');
    cutils = require("cysmeter-utils");
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    debugh = require("debug")("device.historic");

var ReleInfo = {
  operationMode: Number,
  hourOn: Number,
  minuteOn: Number,
  hourOff: Number,
  minuteOff: Number
};

var lmhSchema = {
  type: [Number],
  required: true,
  validator: function validateLength(values){
    return values && (values.length == 3);
  }
};

var lmhHistoricSchema = [{
  created: { type: Date, default: Date.now },
  lmh: lmhSchema,
}];

var monthEnergyValuesSchema = {active:Number,cost:Number,reactive:Number, co2:Number};

var cumulatedEnergyHistoricSchema = {
  key:String,
  value:monthEnergyValuesSchema,
  historic:{type:[{key:String, value:monthEnergyValuesSchema}], select:false}
};

var unitMeasureSchema = {
  name:{type:String, required:true},
  tstamp:{type:Date, default: Date.now},
  pActive:{type:Number, required:true},
  instantCost:{type:Number, required:true}
};

var Device = new Schema({
  name: { type: String, required: true},  //Único dentro de ubicación
  company: {type: ObjectId, required: true, ref: 'Company'},
  gateway: {type: ObjectId, required: true, ref: 'Gateway'},
  location: {type: ObjectId, required: true, ref: 'Location'},
  networkDeviceId: {type: Number, required: true},
  description: {type: String},
  created: { type: Date, default: Date.now },
  enabled: {type: Boolean, default: true},

  powerFreq: { type: Number, required: true},
  energyFreq: { type: Number, required: true},

   // Umbrales de consumo: Va en kwatios, con tres decimales. Potencia activa. Tiene histórico.
  powerLMH: lmhSchema,
  historicPowerLMH: {type:lmhHistoricSchema, select: false},
   // Umbral de gasto: Va en euros. Tiene histórico.
  energyCostLMH: lmhSchema,
  historicEnergyCostLMH: {type:lmhHistoricSchema, select: false},

  expiryPower: {type: Number, required: true},  // 3 meses, por defecto
  expiryEnergy: { type: Number, required: true},  // 3 años, por defecto
  
  header: {type: Boolean, default: false},
  headerDevice: {type: ObjectId, ref: 'Device'},  // Trazabilidad
  
  production:{
    unit: String,
    cysmeterActuationDate: { type: Date, default: Date.now },
    productionInterval: {type:String,enum:['quarter','hour','day','week','month']}
  },

  availablePower: {type: Number, required: true},
  amperageRel: {type: Number, required: true},
  voltageRelA: {type: Number, required: true},
  voltageRelB: {type: Number, required: true},

  alarms:{
    reactivePower:  {type: Number, required: true}, // alarma gestionada en el dispositivo
    activePower:  {type: Number, required: true}, // alarma gestionada en el dispositivo
    activeEnergy:  {type: Number, required: true},  // alarma gestionada en la nube
    reactiveEnergy:  {type: Number, required: true}, // alarma gestionada en la nube
    openAlarms:[{key:String,id:{type: ObjectId, ref: 'Alarm'}}]
  },

  rele1: ReleInfo,
  rele2: ReleInfo,
  
  version: String,
  amperageModel: Number,
  phaseType: {type:String,enum:['B','T']},

  monthEnergyHistoric: cumulatedEnergyHistoricSchema,

  unitMeasures: {type:[unitMeasureSchema], select:false}

/*
Toda esta parametrización se debe enviar y mantener en los dispositivos (menos los de energía)

Potencia disponible: Máxima potencia de ese dispositivo (fondo de escala)
Alarma de potencia reactiva: Se usará para la reactiva de cada fase
Alarma de potencia: Porcentaje sobre fondo de escala
Alarma de energía activa: Máximo que puede gastar en euros al mes (nube)
Alarma de energía reactiva: KWArh. Máximo acumulado en lo que llevas de mes natural (desde el día 1) (nube)
Relación de amperaje: 4 dígitos, intensidad máxima del primario
Relación de voltaje: Una pareja de números que indica la relación.



Unidades que produce
Fecha de corte para comparar mejorar en el coste de producción

*/

});

Device.index({ "location": 1, "name" : 1 }, { unique: true });
Device.index({ "gateway": 1, "networkDeviceId" : 1 }, { unique: true });

utils.consistency (Device, 'gateway');
utils.consistency (Device, 'company');
utils.consistency (Device, 'location');
utils.consistency (Device, 'device', 'headerDevice');

Device.pre('save', function(next) {
  var device = this;
  debugh("Creando dispositivo",device.isModified('powerLMH'),device.isModified('energyCostLMH'));
  if (!device.isModified('powerLMH')){return next();}
  var lmh = device.powerLMH;
  debugh("Creando el historico");
  if (this.isNew){
    device.historicPowerLMH.push({lmh:device.powerLMH});
    next();
  } else {
    model.update({_id:device._id},{$push:{historicPowerLMH:{lmh:lmh}}},function(err){
      debugh("Resultado de la creación",arguments);
      if (err){console.log("Error actualizando histórico de device.powerLMH",err);}
      next();
    });
  }
});

Device.pre('save', function(next) {
  var device = this;
  if (!device.isModified('energyCostLMH')){return next();}
  var lmh = device.energyCostLMH;
  if (this.isNew){
    device.historicEnergyCostLMH.push({lmh:device.energyCostLMH});
    next();
  } else {
    model.update({_id:device._id},{$push:{energyCostLMH:{lmh:lmh}}},function(err){
      debugh("Resultado de la creación",arguments);
      if (err){console.log("Error actualizando histórico de device.energyCostLMH",err);}
      next();
    });
  }
});

Device.statics.findByName = function(name,cb){
  this.findOne({ name: name }, cb);
};

Device.statics.updateById = function(id,update,cb){
  this.update({_id : id}, { $set: update} ,cb);
};

Device.statics.updatePowerLMH = function(id, lmh, cb){
  model.findById(id, function(err, doc){
    if (err){return cb(err);}
    if (doc){
      doc.powerLMH = lmh;
      doc.save(cb);
    } else {cb('Unkown device id',id);}
  });
};

Device.statics.updateEnergyCostLMH = function(id, lmh, cb){
  model.findById(id, function(err, doc){
    if (err){return cb(err);}
    if (doc){
      doc.energyCostLMH = lmh;
      doc.save(cb);
    } else {cb('Unkown device id',id);}
  });
};

Device.statics.incrementEnergyData = function(s, cb){
  var deviceId = s.device, tstamp = s.tstamp, cost = s.energyCost,
      reactive = s.eReactive, active = s.energy;
  console.log("Incrementando", deviceId, active, reactive, cost);
  model.findById(deviceId, "monthEnergyHistoric",function(err, doc){
    if (err){return cb(err);}
    if (doc){
      var m = moment(tstamp);
      var key = m.year()+"-"+(m.month()+1);
      var current = doc.monthEnergyHistoric;
      var oldReactive = 0, oldCost = 0, oldActive = 0;
      if (!current || key != current.key){
        var update={
          "monthEnergyHistoric.key": key,
          "monthEnergyHistoric.value.cost": cost,
          "monthEnergyHistoric.value.active": active,
          "monthEnergyHistoric.value.co2": cutils.kyotoCO2(active),
          "monthEnergyHistoric.value.reactive": reactive,
        };
        if (current){
          update["$push"] = {
            "monthEnergyHistoric.historic":{key: current.key, value: current.value}
          };
        }
        model.update(
          {_id:deviceId},
          update,
          function(err, result){
            cb(err,{result:result,oldReactive:oldReactive,oldCost:oldCost, oldActive:oldActive});
          }
        );
      } else {
        oldReactive = current.value.reactive;
        oldCost = current.value.cost;
        oldActive = current.value.active;
        model.update(
          {_id:deviceId},
          {$inc: {
            "monthEnergyHistoric.value.cost": cost,
            "monthEnergyHistoric.value.active": active,
            "monthEnergyHistoric.value.co2": cutils.kyotoCO2(active),
            "monthEnergyHistoric.value.reactive": reactive}},
          function(err, result){
            cb(err,{result:result, oldReactive:oldReactive, oldCost:oldCost, oldActive:oldActive});
          }
        );
      }
    } else {cb('Unkown device id',deviceId);}
  });
};

Device.statics.addCostCorrection = function(deviceId, correction, tstamp, cb){
  model.findById(deviceId, "monthEnergyHistoric",function(err, doc){
    if (err){return cb(err);}
    if (!doc){cb('Unkown device id',deviceId);}
    var m = moment(tstamp);
    var key = m.year()+"-"+(m.month()+1);
    var current = doc.monthEnergyHistoric;
    if (!current){cb();}
    if (key == current.key){
      model.update(
        {_id:deviceId},
        {$inc: {"monthEnergyHistoric.value.cost": correction}},
        cb
      );
    } else {
      model.update(
        {_id:deviceId, "monthEnergyHistoric.historic.key":key},
        {$inc: {"monthEnergyHistoric.historic.$.value.cost": correction}},
        cb
      );
    }
  });
};

Device.statics.addMeasure = function(id, measure, cb){
  model.update(
    {_id:id, 'unitMeasures.name':{$ne: measure.name}},
    {$push:{unitMeasures:measure}},
    cb);
};

Device.statics.deleteMeasure = function(id, mid, cb){
  model.update({_id:id},{$pull:{unitMeasures:{_id:mid}}}, cb);
};

Device.statics.removeById = function(id,cb){
  this.remove({_id : id},cb);
};

Device.statics.create = function(data, cb){
  x = new model(data);
  console.log("Guardando un dispositivo nuevo");
  x.save(cb);
};

Device.statics.persist = function(data, cb){
  data.save(cb);
};


//Define Models
var model = mongoose.model('Device', Device);

// Export Models
exports.deviceModel = model;

