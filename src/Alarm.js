var mongoose = require('mongoose');
var utils = require('./utils');
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var Alarm = new Schema({
  company: {type: ObjectId, required: true, ref: 'Company'},
  title: {type:String, required:true},
  description: [String],
  created: {type: Date, default: Date.now },
  updated: {type: Date, default: Date.now },
  reviewed: {type: Boolean, default: false},
  reviewedTstamp: {type: Date},
  reviewer: String,
  parameters:[{key:String,value:String}]
});

utils.consistency (Alarm, 'company');

Alarm.statics.filter = function (filter){
  var f = mongoFilter(filter);
  var result = this.find(f).sort(filter.sort);
  if (filter.skip){result = result.skip(filter.skip);}
  if (filter.limit){result = result.limit(filter.limit);}
  return result;
};

Alarm.statics.fcount = function (filter){
  var f = mongoFilter(filter);
  var result = this.count(f);
  return result;
};

var mongoFilter = function(filter){
  if (!filter.sort){filter.sort="-created";}
  var f = {};
  if (filter.start) {f.created = {"$gte":filter.start,"$lt":filter.end};}
  if (filter.text) {
    var pattern = new RegExp(filter.text, "i");
    f['$or']=[{title: {$regex: pattern}},
            {description:{ $regex: pattern}}];
  }
  if (filter.company){ f.company = filter.company;}
  if (filter.notReviewed){ f.reviewed = false;}
  return f;
};



Alarm.statics.gcount = function(filter, cb){
  var company = filter.company;
  var match = {reviewed:false};
  if (company){ match.company = new mongoose.Types.ObjectId(company);}
  var group = {
    _id: "$company",
    company: {$first: "$company"},
    count: { $sum: 1}
  };
  var project = {_id: 0, company: 1, count: 1};
  this.aggregate({$match:match},{$group:group},{$project:project},function(err, result){
    if (err){return cb(err);}
    cb(null, company ? result[0] : result);
  });
};

Alarm.statics.findByName = function(name,cb){
  this.findOne({ name: name }, cb);
};

Alarm.statics.updateById = function(id,update,cb){
  this.update({_id : id}, { $set: update} ,cb);
};

Alarm.statics.removeById = function(id,cb){
  this.remove({_id : id},cb);
};

Alarm.statics.create = function(data, cb){
  x = new model(data);
  x.save(cb);
};

Alarm.statics.persist = function(data, cb){
  data.save(cb);
};

//Define Models
var model = mongoose.model('Alarm', Alarm);

// Export Models
exports.alarmModel = model;

