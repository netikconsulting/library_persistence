var mongoose = require('mongoose');
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

// Period schema
var Interval = new Schema({
// {"month":"6","dstart":"1","dend":"15","hstart":"8","hend":"9","period":"4"},
  month: Number,
  dstart: Number,
  dend: Number,
  hstart: Number,
  hend: Number,
  period: {type:Number, required:true}
});

var Period = new Schema({
  name: { type: String, required: true, unique: true},
  cost: [Number],
  any: [Interval],
  summer: [Interval],
  winter: [Interval],
  working: [Interval]
});


Period.statics.findByName = function(name,cb){
  this.findOne({ name: name }, cb);
};

Period.statics.updateById = function(id,update,cb){
  this.update({_id : id}, {$set: update} ,cb);
};

Period.statics.removeById = function(id,cb){
  this.remove({_id : id},cb);
};

Period.statics.create = function(data, cb){
  x = new model(data);
  x.save(cb);
};

//Define Models
var model = mongoose.model('Period', Period);

// Export Models
exports.periodModel = model;

